//
//  GitHubAPIClient.m
//  Desafio-iOS
//
//  Created by Rodrigo Recio on 09/10/17.
//  Copyright © 2017 Rodrigo Recio. All rights reserved.
//

#import "GitHubAPIClient.h"

#define GITHUB_API_URL @"https://api.github.com"

@implementation GitHubAPIClient

+(GitHubAPIClient *)sharedInstance
{
    static GitHubAPIClient *_sharedInstance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
                      _sharedInstance = [[self alloc] initWithBaseURL:[NSURL URLWithString:GITHUB_API_URL]];
                      _sharedInstance.requestSerializer = [AFJSONRequestSerializer serializer];
                      _sharedInstance.responseSerializer = [AFJSONResponseSerializer serializer];
                  });
    
    return _sharedInstance;
}

- (void)searchRepositoriesWithPage:(NSUInteger)pageNumber
                           success:(void (^) (NSArray *repos))successBlock
                             error:(void (^) (NSURLSessionTask *operation, NSError *error))errorBlock {
    
    NSDictionary *parms = @{@"q":@"language:Java", @"sort":@"stars", @"page":@(pageNumber)};
    
    [self GET:@"/search/repositories" parameters:parms progress:nil
      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
          NSError *error = nil;
          NSArray *repos = [MTLJSONAdapter modelsOfClass:GHRepository.class fromJSONArray:responseObject[@"items"] error:&error];
          if (!error) {
              successBlock(repos);
          } else {
              errorBlock(task, error);
          }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock(task, error);
    }];
}

- (void)pullRequestsWithAuthor:(NSString *)author
                repositoryName:(NSString *)repoName
                       success:(void (^) (NSArray *pullRequests))successBlock
                         error:(void (^) (NSURLSessionTask *operation, NSError *error))errorBlock {

    NSString *path = [NSString stringWithFormat:@"/repos/%@/%@/pulls", author, repoName];
    
    [self GET:path parameters:nil progress:nil
      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
          NSError *error = nil;
          NSArray *repos = [MTLJSONAdapter modelsOfClass:GHPullRequest.class fromJSONArray:responseObject error:&error];
          if (!error) {
              successBlock(repos);
          } else {
              errorBlock(task, error);
          }
      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
          errorBlock(task, error);
      }];
}

@end
