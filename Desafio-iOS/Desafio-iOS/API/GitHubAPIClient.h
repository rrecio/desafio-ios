//
//  GitHubAPIClient.h
//  Desafio-iOS
//
//  Created by Rodrigo Recio on 09/10/17.
//  Copyright © 2017 Rodrigo Recio. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

#import "GHPullRequest.h"
#import "GHRepository.h"
#import "GHUser.h"

@interface GitHubAPIClient : AFHTTPSessionManager

+(GitHubAPIClient *)sharedInstance;

- (void)searchRepositoriesWithPage:(NSUInteger)pageNumber
                           success:(void (^) (NSArray *repos))successBlock
                             error:(void (^) (NSURLSessionTask *operation, NSError *error))errorBlock;

- (void)pullRequestsWithAuthor:(NSString *)author
                repositoryName:(NSString *)repoName
                       success:(void (^) (NSArray *pullRequests))successBlock
                         error:(void (^) (NSURLSessionTask *operation, NSError *error))errorBlock;

@end
