//
//  MasterViewController.m
//  Desafio-iOS
//
//  Created by Rodrigo Recio on 08/10/17.
//  Copyright © 2017 Rodrigo Recio. All rights reserved.
//

#import "MasterViewController.h"
#import "PullRequestsTableViewController.h"

// API
#import "GitHubAPIClient.h"

// Models
#import "GHRepository.h"

// Views
#import "RepositoryTableViewCell.h"

@interface MasterViewController ()

@property NSArray *objects;
@property NSUInteger pageNumber;
@end

@implementation MasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.detailViewController = (PullRequestsTableViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    self.objects = @[];
    self.pageNumber = 0;
    [self loadRepos];
}

- (void)loadRepos {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [[GitHubAPIClient sharedInstance] searchRepositoriesWithPage:self.pageNumber success:^(NSArray *repos) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        self.objects = [self.objects arrayByAddingObjectsFromArray:repos];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    } error:^(NSURLSessionTask *operation, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        NSLog(@"Error: %@", error);
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        GHRepository *object = self.objects[indexPath.row];
        PullRequestsTableViewController *controller = (PullRequestsTableViewController *)[[segue destinationViewController] topViewController];
        [controller setRepository:object];
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.objects.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    RepositoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    // TODO: move all this code into it's own UITableViewCell subclass
    GHRepository *object = self.objects[indexPath.row];
    [cell configWithRepository:object];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == (self.objects.count-1)) {
        self.pageNumber += 1;
        [self loadRepos];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

@end
