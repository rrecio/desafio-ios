//
//  MasterViewController.h
//  Desafio-iOS
//
//  Created by Rodrigo Recio on 08/10/17.
//  Copyright © 2017 Rodrigo Recio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PullRequestsTableViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) PullRequestsTableViewController *detailViewController;


@end

