//
//  main.m
//  Desafio-iOS
//
//  Created by Rodrigo Recio on 08/10/17.
//  Copyright © 2017 Rodrigo Recio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
