//
//  PullRequestTableViewCell.m
//  Desafio-iOS
//
//  Created by Rodrigo Recio on 09/10/17.
//  Copyright © 2017 Rodrigo Recio. All rights reserved.
//

#import "PullRequestTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation PullRequestTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configWithPullRequest:(GHPullRequest *)pullRequest {
    self.mainTitleLabel.text = pullRequest.title;
    self.dateLabel.text = [[self dateFormatter] stringFromDate:pullRequest.createdAt];
    self.bodyLabel.text = pullRequest.body;
    [self.avatarImageView sd_setImageWithURL:pullRequest.user.avatarURL placeholderImage:[UIImage imageNamed:@"placeholder"]];
    self.usernameLabel.text = pullRequest.user.login;
}

- (NSDateFormatter *)dateFormatter {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    return dateFormatter;
}

@end
