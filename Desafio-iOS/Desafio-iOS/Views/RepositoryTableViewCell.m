//
//  RepositoryTableViewCell.m
//  Desafio-iOS
//
//  Created by Rodrigo Recio on 09/10/17.
//  Copyright © 2017 Rodrigo Recio. All rights reserved.
//

#import "RepositoryTableViewCell.h"
#import "GHRepository.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation RepositoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configWithRepository:(GHRepository *)repository {
    self.nameLabel.text = repository.name;
    self.descriptionLabel.text = repository.repositoryDescription;
    [self.avatarImageView sd_setImageWithURL:[repository.owner avatarURL]
                            placeholderImage:[UIImage imageNamed:@"placeholder"]];
    self.forksLabel.text = [NSString stringWithFormat:@"⑂ %@", repository.forksCount.description];
    self.starsLabel.text = [NSString stringWithFormat:@"★ %@", repository.stargazersCount.description];
    self.usernameLabel.text = repository.owner.login;
}

@end
