//
//  PullRequestsTableViewController.h
//  Desafio-iOS
//
//  Created by Rodrigo Recio on 09/10/17.
//  Copyright © 2017 Rodrigo Recio. All rights reserved.
//

#import <UIKit/UIKit.h>
@class GHRepository;
@interface PullRequestsTableViewController : UITableViewController
@property (nonatomic, assign) GHRepository *repository;
- (void)setRepository:(GHRepository *)repository;
@end
