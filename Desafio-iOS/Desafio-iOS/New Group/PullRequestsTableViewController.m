//
//  PullRequestsTableViewController.m
//  Desafio-iOS
//
//  Created by Rodrigo Recio on 09/10/17.
//  Copyright © 2017 Rodrigo Recio. All rights reserved.
//

#import "PullRequestsTableViewController.h"
#import "GitHubAPIClient.h"
#import "PullRequestTableViewCell.h"
#import <SafariServices/SafariServices.h>

@interface PullRequestsTableViewController ()
@property NSArray *pullRequests;
@end

@implementation PullRequestsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setRepository:(GHRepository *)repository {
    [self loadPullRequestsWithRepository:repository];
}

- (void)loadPullRequestsWithRepository:(GHRepository *)repository {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [[GitHubAPIClient sharedInstance] pullRequestsWithAuthor:repository.owner.login repositoryName:repository.name success:^(NSArray *pullRequests) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        self.pullRequests = pullRequests;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    } error:^(NSURLSessionTask *operation, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        NSLog(@"Error loading pull requests: %@", error);
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.pullRequests.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PullRequestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PullRequestCell" forIndexPath:indexPath];
    
    // Configure the cell...
    GHPullRequest *pullRequest = self.pullRequests[indexPath.row];
    [cell configWithPullRequest:pullRequest];
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    GHPullRequest *pr = self.pullRequests[indexPath.row];
    if (@available(iOS 9.0, *)) {
        SFSafariViewController *safari = [[SFSafariViewController alloc] initWithURL:pr.HTMLURL];
        [self presentViewController:safari animated:YES completion:nil];
    } else {
        // Fallback on earlier versions
        [[UIApplication sharedApplication] openURL:pr.HTMLURL];
    }
}

@end
