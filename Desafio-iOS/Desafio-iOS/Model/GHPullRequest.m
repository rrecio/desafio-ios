//
//  GHPullRequest.m
//  Desafio-iOS
//
//  Created by Rodrigo Recio on 09/10/17.
//  Copyright © 2017 Rodrigo Recio. All rights reserved.
//

#import "GHPullRequest.h"

@implementation GHPullRequest

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"user": @"user",
             @"title": @"title",
             @"body": @"body",
             @"createdAt": @"created_at",
             @"HTMLURL": @"html_url"};
}

+ (NSValueTransformer *)userJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:GHUser.class];
}

+ (NSValueTransformer *)createdAtJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *dateString, BOOL *success, NSError *__autoreleasing *error) {
        return [self.dateFormatter dateFromString:dateString];
    } reverseBlock:^id(NSDate *date, BOOL *success, NSError *__autoreleasing *error) {
        return [self.dateFormatter stringFromDate:date];
    }];
}

+ (NSDateFormatter *)dateFormatter {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss'Z'";
    return dateFormatter;
}

@end
