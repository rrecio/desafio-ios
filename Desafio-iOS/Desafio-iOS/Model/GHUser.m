//
//  GHUser.m
//  Desafio-iOS
//
//  Created by Rodrigo Recio on 09/10/17.
//  Copyright © 2017 Rodrigo Recio. All rights reserved.
//

#import "GHUser.h"

@implementation GHUser

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"login": @"login",
             @"userId": @"id",
             @"avatarURL": @"avatar_url"};
}

@end
