//
//  GHRepository.h
//  Desafio-iOS
//
//  Created by Rodrigo Recio on 09/10/17.
//  Copyright © 2017 Rodrigo Recio. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "GHUser.h"

@interface GHRepository : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSNumber *repositoryId;
@property (nonatomic, strong, readonly) GHUser *owner;
@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, copy, readonly) NSString *repositoryDescription;
@property (nonatomic, copy, readonly) NSNumber *forksCount;
@property (nonatomic, copy, readonly) NSNumber *stargazersCount;

@end
