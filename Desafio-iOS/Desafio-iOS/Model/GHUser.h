//
//  GHUser.h
//  Desafio-iOS
//
//  Created by Rodrigo Recio on 09/10/17.
//  Copyright © 2017 Rodrigo Recio. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface GHUser : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSString *login;
@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, copy, readonly) NSNumber *userId;
@property (nonatomic, copy, readonly) NSURL *avatarURL;

@end

