//
//  GHPullRequest.h
//  Desafio-iOS
//
//  Created by Rodrigo Recio on 09/10/17.
//  Copyright © 2017 Rodrigo Recio. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "GHUser.h"

@interface GHPullRequest : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) GHUser *user;
@property (nonatomic, copy, readonly) NSString *title;
@property (nonatomic, copy, readonly) NSString *body;
@property (nonatomic, copy, readonly) NSDate *createdAt;
@property (nonatomic, copy, readonly) NSURL *HTMLURL;

@end
