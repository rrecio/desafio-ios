//
//  GHRepository.m
//  Desafio-iOS
//
//  Created by Rodrigo Recio on 09/10/17.
//  Copyright © 2017 Rodrigo Recio. All rights reserved.
//

#import "GHRepository.h"

@implementation GHRepository

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"repositoryId": @"id",
             @"owner": @"owner",
             @"name": @"name",
             @"repositoryDescription": @"description",
             @"forksCount": @"forks_count",
             @"stargazersCount": @"stargazers_count"};
}

+ (NSValueTransformer *)ownerJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:GHUser.class];
}

@end
