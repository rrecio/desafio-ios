//
//  Desafio_iOSTests.m
//  Desafio-iOSTests
//
//  Created by Rodrigo Recio on 08/10/17.
//  Copyright © 2017 Rodrigo Recio. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "GitHubAPIClient.h"

@interface Desafio_iOSTests : XCTestCase
@property (strong, nonatomic) GitHubAPIClient *apiClient;
@end

@implementation Desafio_iOSTests

- (void)setUp {
    [super setUp];
    self.apiClient = [GitHubAPIClient sharedInstance];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testRepositoriesRetrieval {
    XCTestExpectation *expectation = [self expectationWithDescription:@"Load Repositories using GitHubAPIClient"];
    
    [self.apiClient searchRepositoriesWithPage:0 success:^(NSArray *repos) {
        XCTAssertNotNil(repos, @"Repos are nil when they were supposed to bring 30 results");
        XCTAssertTrue(repos.count == 30, @"Repos array is empty when they were supposed to bring 30 results");
        [expectation fulfill];
    } error:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error loading repositories: %@", error);
    }];
    
    [self waitForExpectationsWithTimeout:5.0
                                 handler:^(NSError *error) {
                                     if (error) {
                                         NSLog(@"Timeout Error: %@", error);
                                     }
                                 }];
}

- (void)testPullRequestsTetrieval {
    XCTestExpectation *expectation = [self expectationWithDescription:@"Load Pull Requests using GitHubAPIClient"];
    
    [self.apiClient pullRequestsWithAuthor:@"apple" repositoryName:@"swift" success:^(NSArray *pullRequests) {
        XCTAssertNotNil(pullRequests, @"PullRequests are nil");
        XCTAssertTrue(pullRequests.count > 0, @"PullRequests array is empty");
        [expectation fulfill];
    } error:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error loading pull requests: %@", error);
    }];
    
    [self waitForExpectationsWithTimeout:5.0
                                 handler:^(NSError *error) {
                                     if (error) {
                                         NSLog(@"Timeout Error: %@", error);
                                     }
                                 }];
}

@end
